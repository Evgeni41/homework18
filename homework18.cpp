﻿#include <iostream>

template<typename T>
class Stack
{
private:
    int size = 0;
    T* stack = nullptr;
public:
    Stack(int _size)
    {
        size = _size;
        stack = new T[size];
        std::cout << "Input elements: " << std::endl;
        for (int i = 0; i < size; ++i)
            std::cin >> stack[i];
        std::cout << "Done!" << std::endl;
    }

    void pop()
    {
        --size;
        T* newStack = new T[size];
            for (int i = 0; i < size; ++i)
            {
                newStack[i] = stack[i];
            }

            delete[] stack;
            stack = newStack;
    }

    void push(T _value)
    {
        T* newStack = new T[size + 1];

        for (int i = 0; i < size; ++i)
        {
            newStack[i] = stack[i];
        }

        newStack[size] = _value;

        ++size;
        delete[] stack;
        stack = newStack;
    }

    void ShowStack()
    {
        for (int i = 0; i < size; ++i)
        {
            std::cout << stack[i] << " ";
        }
        std::cout << std::endl;
    }
};

int main()
{
    Stack<std::string> data(3);
    data.ShowStack();
    data.pop();
    data.ShowStack();
    data.push("yellow");
    data.push("world");
    data.ShowStack();
    return 0;
}

